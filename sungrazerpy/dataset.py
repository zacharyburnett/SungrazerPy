from datetime import date


class LASCOimagery:
    """
    imagery from the Large Angle and Spectrometric Coronagraph Experiment (LASCO) instrument aboard the Solar-Orbiting Heliophysics Observatory (SOHO)
    https://lasco-www.nrl.navy.mil/
    """

    # imagery from the last 16 days is stored here
    LEVEL_05_URL = 'https://umbra.nascom.nasa.gov/pub/lasco/lastimage/level_05'

    header = [
        'file_name',
        'observation_date',
        'observation_time',
        'tel',
        'exposure_time',
        'cols',
        'rows',
        'starting_col',
        'starting_row',
        'filter_1',
        'filter_2',
        'filter_3',
        'polar',
        'leb_program'
    ]

    def __init__(self, imagery_date: date):
        self.date = imagery_date
        self.images = []

    @property
    def url(self) -> str:
        return f'{self.LEVEL_05_URL}/{self.date:%y%m%d}'
