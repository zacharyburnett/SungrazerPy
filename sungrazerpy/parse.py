import re

from astropy.io import fits
import requests


def url_links(url: str, complete: bool = True) -> [str]:
    url = url.strip('/')
    links = []
    for link in re.finditer('href="(.+?)"', requests.get(url).text):
        link = link.group(1)
        if complete and '://' not in link:
            link = f'{url}/{link}'
        links.append(link)
    return links


def url_local_links(url: str, complete: bool = True) -> [str]:
    url = url.strip('/')
    local_links = []
    for link in url_links(url, complete=False):
        if link[0] in ['?', '/']:
            continue
        if '://' not in link:
            if '/' in link:
                link = link.split('/', 1)[0]
            if complete:
                link = f'{url}/{link}'
            local_links.append(link)
    return local_links


def url_tree(url: str, levels: int = None) -> {str: {}}:
    tree = {url: {}}
    if levels is not None:
        levels -= 1
    if levels is None or levels >= 0:
        for link in url_local_links(url):
            if '.' in link.rsplit('/', 1)[-1] and not link.endswith('html'):
                link_tree = None
            else:
                link_tree = url_tree(link, levels)[link]
            link = link.replace(url, '').strip('/')
            if levels == 1:
                link_tree = list(link_tree)
            tree[url][link] = link_tree
    return tree


if __name__ == '__main__':
    # root_url = 'https://umbra.nascom.nasa.gov/pub/lasco/lastimage/level_05/'
    # tree = url_tree(url, 2)

    fits_image_filename = fits.util.get_testdata_filepath('test0.fits')
    hdul = fits.open(fits_image_filename)

    print('done')
